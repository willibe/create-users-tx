# Dette scriptet er veldig inspirert av Erik sitt "CreateUsersCSV.ps1" script.
# Endringene vi har gjort er aa gjore navnene mer internasjonale, siden vaar
# fiktive bedrift er en internasjonal bedrift. Dette scriptet lager en liste
# med 100 ansatte som blir tilfeldig fordelt utover avdelingene.
#

if ((Get-ChildItem -ErrorAction SilentlyContinue teamXtremeUsers.csv).Exists)
  {"You alread have the file teamXtremeUsers.csv!"; return;}

# 100 unike amerikanske fornavn
#
$FirstName = @("Mary","Patricia","Jennifer","Linda","Elizabeth","Barbara","Susan","Jessica",
               "Sarah","Karen","Nancy","Lisa","Betty","Margaret","Sandra","Ashley",
               "Kimberly","Emily","Donna","Michelle","Dorothy","Carol","Amanda","Melissa","Deborah",
               "Stephanie","Rebecca","Sharon","Laura","Cynthia","Kathleen","Amy",
               "Shirley","Angela","Helen","Anna","Brenda","Pamela","Nicole",
               "Emma","Samantha","Katherine","christine","Debra","Rachel","Catherine",
               "Carolyn","Janet","Ruth","Maria","James","Robert","Michael","William",
               "David","Richard","Joseph","Thomas","Charles","Christopher","Daniel",
               "Matthew","Anthony","Mark","Donald","Steven","Paul","Andrew",
               "Joshua","Kenneth","Kevin","Brian","George","Edward","Ronald",
               "Timothy","Jason","Jeffery","Ryan","Jacob","Gary",
               "Nicholas","Eric","Jonathan","Stephen","Larry","Justin","Scott",
               "Brandon","Benjamin","Samuel","Georgy","Frank","Alexander","Raymond",
               "Patrick","Jack","Dennis","Jerry","Tyler"
              )

# 100 unike amerikasnke etternavn
#
$LastName = @("Smith","Johnson","Williams","Brown","Jones","Garcia",
              "Miller","Davis","Rodriguez","Martinez","Hernandez","Lopez",
              "Gonzales","Wilson","Anderson","Moore","Jackson","White",
              "Harris","Sanchez","Clark","Ramirez","Lewis","Robinson",
              "Walker","Young","Allen","King","Wright","Scott",
              "Torres","Nquyen","Hill","Flores","Green","Adams",
              "Nelson","Baker","Hall","Rivera","Campbell","Mitchell",
              "Carter","Roberts","Gomez","Phillips","Evans","Turner","Diaz",
              "Parker","Cruz","Edwards","Collins","Reyes","Stewart",
              "Morris","Morales","Murphy","Cook","Rogers","Gutierrez",
              "Ortiz","Morgan","Cooper","Peterson","Bailey","Reed",
              "Kelly","Howard","Ramos","Kim","Cox",
              "Ward","Richardson","Watson","Brooks","Chavez","Wood","Bennet",
              "Gray","Mendoza","Ruiz","Hughes","Price","Alvarez",
              "Castillo","Sanders","Patel","Myers","Long","Ross",
              "Foster","Jimenez","Marshall","Owens","Harrison","Woods","Shaw",
              "Dixon","Hunt"
             )

# 10 i HR, 10 i Adm, 30 Defend Konsulenter, 30 Offensiv Konsulenter,
# 10 i Ekstern finans og 10 i Intern finans.
#
$OrgUnits = @("ou=HR,ou=AllUsers","ou=HR,ou=AllUsers",
              "ou=HR,ou=AllUsers","ou=HR,ou=AllUsers",
              "ou=HR,ou=AllUsers","ou=IT,ou=AllUsers",
              "ou=IT,ou=AllUsers","ou=IT,ou=AllUsers",
              "ou=IT,ou=AllUsers","ou=IT,ou=AllUsers",
              "ou=Adm,ou=AllUsers","ou=Adm,ou=AllUsers",
              "ou=Adm,ou=AllUsers","ou=Adm,ou=AllUsers",
              "ou=Adm,ou=AllUsers","ou=Adm,ou=AllUsers",
              "ou=Adm,ou=AllUsers","ou=Adm,ou=AllUsers",
              "ou=Adm,ou=AllUsers","ou=Adm,ou=AllUsers",
              "ou=EkstFin,ou=Fin,ou=AllUsers","ou=EkstFin,ou=Fin,ou=AllUsers",
              "ou=EkstFin,ou=Fin,ou=AllUsers","ou=EkstFin,ou=Fin,ou=AllUsers",
              "ou=EkstFin,ou=Fin,ou=AllUsers","ou=EkstFin,ou=Fin,ou=AllUsers",
              "ou=EkstFin,ou=Fin,ou=AllUsers","ou=EkstFin,ou=Fin,ou=AllUsers",
              "ou=EkstFin,ou=Fin,ou=AllUsers","ou=EkstFin,ou=Fin,ou=AllUsers",
              "ou=IntFin,ou=Fin,ou=AllUsers","ou=IntFin,ou=Fin,ou=AllUsers",
              "ou=IntFin,ou=Fin,ou=AllUsers","ou=IntFin,ou=Fin,ou=AllUsers",
              "ou=IntFin,ou=Fin,ou=AllUsers","ou=IntFin,ou=Fin,ou=AllUsers",
              "ou=IntFin,ou=Fin,ou=AllUsers","ou=IntFin,ou=Fin,ou=AllUsers",
              "ou=IntFin,ou=Fin,ou=AllUsers","ou=IntFin,ou=Fin,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=OffKons,ou=Kons,ou=AllUsers","ou=OffKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers",
              "ou=DefKons,ou=Kons,ou=AllUsers","ou=DefKons,ou=Kons,ou=AllUsers"
             )

# Lager tilfeldige navn, etternavn og tildeler en avdeling.
#
$fnidx = 0..99 | Get-Random -Shuffle
$lnidx = 0..99 | Get-Random -Shuffle
$ouidx = 0..99 | Get-Random -Shuffle

Write-Output "UserName;GivenName;SurName;UserPrincipalName;DisplayName;Password;Department;Path" > teamXtremeUsers.csv

foreach ($i in 0..99) {
  $UserName          = $FirstName[$fnidx[$i]].ToLower()
  $GivenName         = $FirstName[$fnidx[$i]]
  $SurName           = $LastName[$lnidx[$i]]
  $UserPrincipalName = $UserName + '@' + 'team.xtreme.com'
  $DisplayName       = $GivenName + ' ' + $SurName
  $Password          = -join ('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ0123456789!"#$%&()*+,-./:<=>?@[\]_{|}'.ToCharArray() | Get-Random -Count 16)
  $Department        = ($OrgUnits[$ouidx[$i]] -split '[=,]')[1]
  $Path              = $OrgUnits[$ouidx[$i]] + ',' + "dc=team,dc=xtreme"
  Write-Output "$UserName;$GivenName;$SurName;$UserPrincipalName;$DisplayName;$Password;$Department;$Path" >> teamXtremeUsers.csv
}


#$UserName          = "Michael.Scarn"
#$GivenName         = "Michael"
#$SurName           = "Scott"
#$UserPrincipalName = $UserName + '@' + 'team.xtreme.com'
#$DisplayName       = $GivenName + ' ' + $SurName
#$Password          = -join ('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRSTUVWXYZ0123456789!"#$%&()*+,-./:<=>?@[\]_{|}'.ToCharArray() | Get-Random -Count 16)
#$Department        = "CEO"
#$Path              = "ou=CEO" + "ou=AllUsers" + ',' + "dc=team,dc=xtreme"
#Write-Output "$UserName;$GivenName;$SurName;$UserPrincipalName;$DisplayName;$Password;$Department;$Path" >> teamXtremeUsers.csv
